/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import util.Cola;
import util.Pila;

/**
 *
 * @author Jeison Damian Murillo Sepulveda_1151979
 */
public class Evaluador {
    
    public double evaluar(String infija){
        String posfija = convertirPosfijo(infija);
        
        return evaluarPosfija(posfija);
    }

    public String convertirPosfijo(String infija) {
        String posfija = "";
        Pila pila = new Pila();
        for(int i=0; i<infija.length();i++){
            char letra = infija.charAt(i);
            if(esOperador(letra)){
                if(pila.esVacia()) {
                    pila.apilar(letra);
                } else {
                    int pe = prioridadEnExpresion(letra);
                    int pp = prioridadEnPila((char) pila.getTope());
                    if(pe>pp){
                        pila.apilar(letra);
                    } else {
                        posfija += pila.desapilar();
                        pila.apilar(letra);
                    }
                }
            } else {
                posfija += letra;
            }
        } 
        while(!pila.esVacia()){
            posfija += pila.desapilar();
        }
        return posfija;
    }
    
    public String convertirPrefijo(String infijo) {

        Pila<String> numeros = new Pila();
        Pila<String> operadores = new Pila();
        Cola<String> agrup = this.ConvertirAToken(infijo);
        int tamano = agrup.getTamano();
        String prefijo = "";
        
        for (int i = 0; i < tamano; i++) {
            String prueba = agrup.consultar();
            if (prueba.equals("(")) {
                operadores.apilar(agrup.deColar());
            } else if (prueba.equals(")")) {
                while (!operadores.esVacia() && !operadores.getTope().equals("(")) {
                    agrup.deColar();
                    String numero1 = numeros.desapilar();
                    String numero2 = numeros.desapilar();
                    String operador = operadores.desapilar();
                    prefijo = (operador + numero2 + numero1);
                    numeros.apilar(prefijo);
                }
                operadores.desapilar();
            } else if (!esOperador(prueba.charAt(0))) {
                numeros.apilar(agrup.deColar());
            } else {
                while (!operadores.esVacia() && prioridadEnExpresion(prueba.charAt(0))
                        <=prioridadEnExpresion(operadores.getTope().charAt(0))) {
                    String numero1 = numeros.desapilar();
                    String numero2 = numeros.desapilar();
                    String operador = operadores.desapilar();
                    prefijo = (operador + numero2 + numero1);
                    numeros.apilar(prefijo);
                    
                }
                operadores.apilar(agrup.deColar());
            }
        }
        while (!operadores.esVacia()) {
                String numero1 = numeros.desapilar();
                String numero2 = numeros.desapilar();
                String operador = operadores.desapilar();
                prefijo = (operador + numero2 + numero1);
                numeros.apilar(prefijo);
            }
        return numeros.desapilar();
    } 
 
    
    private int prioridadEnExpresion(char operador){
        if(operador == '^') return 4;
        if(operador == '*' ||  operador == '/') return 2;
        if(operador == '+' || operador == '-') return 1;
        if(operador == '(') return 5;
        return 0;
    }
    
    private int prioridadEnPila(char operador){
        if(operador == '^') return 3;
        if(operador == '*' ||  operador == '/') return 2;
        if(operador == '+' || operador == '-') return 1;
        if(operador == '(') return 0;
        return 0;
    }

    private double evaluarPosfija(String posfija) {
        Pila pila = new Pila();
        for(int i=0; i<posfija.length(); i++){
            char letra = posfija.charAt(i);
            if(!esOperador(letra)) {
                double num = new Double(letra+"");
                pila.apilar(num);
            } else {
                double num2 = (double) pila.desapilar();
                double num1 = (double) pila.desapilar();
                double num3 = operacion(letra, num1, num2);
                pila.apilar(num3);
            }
        }
        return (double)pila.getTope();
    }

    private boolean esOperador(char letra) {
        if(letra=='*' || letra=='/' || letra=='+' || letra=='-'|| letra=='(' || letra==')' || letra=='^'){
            return true;
        }
        return false;
    }

    private double operacion(char letra, double num1, double num2) {
        if(letra=='*') return num1 * num2;
        if(letra=='/') return num1 / num2;
        if(letra=='+') return num1 + num2;
        if(letra=='-') return num1 - num2;
        if(letra=='^') return Math.pow(num1,num2);
        return 0;
    }
    
    public Cola ConvertirAToken(String infija) {

        Cola<String> tokens = new Cola();
        String aux = "";

        for (int i = 0; i < infija.length(); i++) {

            char letra = infija.charAt(i);

            if (esOperador(letra)) {
                tokens.enColar(letra + "");
                int tmp = i + 1;
                if (infija.charAt(tmp) == '-' || infija.charAt(tmp) == '+') {
                    aux += (infija.charAt(tmp) + "");
                    i++;
                }
            } else if (!esOperador(letra) && letra != ')') {
                aux += (letra + "");
                int tmp = i + 1;
                while (tmp < infija.length() && infija.charAt(tmp) != ')' && !esOperador(infija.charAt(tmp))) {
                    aux += (infija.charAt(tmp) + "");
                    tmp++;
                    i++;
                }
                tokens.enColar(aux);
                aux = "";
            } else if (letra == ')') {
                tokens.enColar(letra + "");
            }
        }
        return tokens;
    }
}
