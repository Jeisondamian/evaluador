/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * A través de listasCD
 * @author madar
 */
public class Cola<T> {
    
    private ListaCD<T> tope;

    public Cola() {
        this.tope=new ListaCD();
    }
    
    public void enColar(T elemento)
    {
        this.tope.insertarFin(elemento);
    }
    
    public T deColar()
    {
        return this.tope.eliminar(0);
    }
    
    public boolean esVacia()
    {
        return this.tope.esVacia();
    }
    
    public int getTamano()
    {
        return this.tope.getTamano();
    }
    
    public void vaciar(){
        tope.vaciar();
    }
    
    public T consultar(){
        return this.tope.get(0);
    }
    
    @Override
    public String toString() {
        return tope.toString();
    }
    
    
    
}
